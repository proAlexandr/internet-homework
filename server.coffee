require './lib/date-extention'

express = require 'express'
cookieParser = require 'cookie-parser'
useragent = require 'express-useragent'
connectAssets = require "connect-assets"
path = require 'path'
mongo = require 'mongodb'
monk = require 'monk'
bodyParser = require 'body-parser'
sanitizeHtml = require 'sanitize-html'
md5 = require 'MD5'
gm = require 'gm'

PORT = 3000
MONGO_DATABASE = 'localhost:27017/internet-homework'
COOKIE_VISIT_NAME = 'last-visit'
COLLECTION_NAME_COUNTER = 'counter'
COLLECTION_NAME_TOKENS = 'tokens'
COLLECTION_NAME_FEEDBACK = 'feedback'
COUNTER_FILE = 'public/count.png'

# Конфигурирование
#
db = monk MONGO_DATABASE
Counter = db.get COLLECTION_NAME_COUNTER
Tokens = db.get COLLECTION_NAME_TOKENS
Feedback = db.get COLLECTION_NAME_FEEDBACK
Interview = db.get 'interview'

app = express()
app.set 'views', __dirname + '/views'
app.set 'view engine', 'jade'
app.use connectAssets()
app.use cookieParser()
app.use express.static(path.join(__dirname, '/public'))
app.use bodyParser()

# Инициализация
#
(->
  Counter.findOne {}, {}, (e, docs) ->
    Counter.insert value: 0 if !docs?
  Interview.findOne {}, {}, (e, docs) ->
    Interview.insert {} if !docs?
)()

# Счетчик посещений
#
app.use  (req, res, next) ->

  Counter.findOne {}, {}, (e, doc) ->
    ua = if req.headers['user-agent'] then useragent.parse req.headers['user-agent'] else undefined
    isGoodUserAgent = ua && ua.Browser != 'unknown'

    ipv4 = req.headers['x-real-ip'] || req.connection.remoteAddress
    res.promakh =
      visitCounter: doc.value
      ua: ua

    return next() if req.path == '/counter.png'

    tokenValue = md5(ipv4 + JSON.stringify(ua.Browser))

    Tokens.findOne {value: tokenValue}, {}, (e, document) ->
      if document?
        res.promakh.token = document
        if (new Date()).getDate() != document.last_visit.getDate()
          Counter.update {}, { value: doc.value + 1 }
          res.promakh.visitCounter += 1
          res.promakh.token.visits_sum += 1
          res.promakh.token.visits_today = 1
          res.promakh.token.last_visit = new Date()
        if Math.abs(res.promakh.token.last_visit - new Date()) > 5 * 1000
          res.promakh.token.visits_today += 1
        if res.promakh.token.visits_pages[res.promakh.token.visits_pages.length - 1] != req.path
          res.promakh.token.visits_pages.push req.path
        res.promakh.token.last_visit = new Date()
        Tokens.update {value: tokenValue}, res.promakh.token
      else
        if isGoodUserAgent
          res.promakh.token =
            value: tokenValue
            visits_today: 1
            visits_sum: 1
            last_visit: new Date()
            visits_pages: [req.path]
            ip4: ipv4
            ua: ua
          Tokens.insert res.promakh.token, ->
            Counter.update {}, { value: doc.value + 1 }
            res.promakh.visitCounter += 1
      next()

app.param 'feedbackId', (req, res, next, feedbackId) ->
  req.feedbackId = feedbackId
  next()

app.get '/', (req, res) ->
  res.render 'index', title: 'Home', promakh: res.promakh

app.get '/gallery', (req, res) ->
  res.render 'gallery', title: 'Gallery', promakh: res.promakh

app.get '/feedback', (req, res) ->
  Feedback.find {}, {sort: {_id: -1}}, (error, collection) ->
    res.render 'feedback',
      title: 'Feedback'
      promakh: res.promakh
      collection: collection
      capcha: Math.round(Math.random() * 1000000)
      item: {}

app.get '/feedback/:feedbackId', (req, res) ->
  try
    Feedback.findOne { _id: mongo.ObjectID(req.feedbackId) }, (error, document) ->
      res.render 'feedback_edit',
        title: 'Feedback'
        promakh: res.promakh
        capcha: Math.round(Math.random() * 1000000)
        resource: document
  catch
    res.send "Error, invalid object id"

app.post '/feedback', (req, res) ->
  content = sanitizeHtml req.body.content,
    allowedTags: [ 'b', 'i', 'img']
    allowedAttributes:
      'img': [ 'src' ]

  item =
    versions: [{
      content: content
    }]
    author_token: res.promakh.token.value

  if req.body.expect_capcha == req.body.capcha
    Feedback.insert item, ->
      res.redirect '/feedback'
  else
    Feedback.find {}, {sort: {_id: -1}}, (error, collection) ->
      res.render 'feedback',
        title: 'Feedcack'
        promakh: res.promakh
        collection: collection
        capcha: Math.round(Math.random() * 1000000)
        item: item
        error: "Invalid capcha"

app.post '/feedback/:feedbackId', (req, res) ->
  Feedback.findOne { _id: mongo.ObjectID(req.feedbackId) }, (error, document) ->
    resource = document
    content = sanitizeHtml req.body.content,
      allowedTags: [ 'b', 'i', 'img']
      allowedAttributes:
        'img': [ 'src' ]

    resource.versions.unshift {content: content}

    if req.body.expect_capcha == req.body.capcha
      Feedback.update { _id: resource._id }, resource, ->
        res.redirect '/feedback'
    else
      res.render 'feedback_edit',
        title: 'Feedback'
        promakh: res.promakh
        capcha: Math.round(Math.random() * 1000000)
        resource: resource
        error: "Invalid capcha"

app.get '/admin', (req, res) ->
  Tokens.find {}, (error, collection) ->
    res.render 'admin', title: 'Admin', promakh: res.promakh, tokens: collection


convertInterviewToValues = (doc) ->
  sum = 1
  result = {}
  for i in [1..8]
    a = doc[i] || 0
    sum += a
  for i in [1..8]
    result[i] = "width:#{(doc[i] || 0) / sum * 100}%"
  return result


app.get '/interview', (req, res) ->
  if req.xhr
    Interview.findOne {}, {}, (e, doc) ->
      res.render 'interview-result', values: convertInterviewToValues(doc)
  else
    res.render 'interview', title: 'Interview', promakh: res.promakh

app.post '/interview', (req, res) ->
  vote = parseInt req.body.vote

  if !res.promakh.token.voted && [1,2,3,4,5,6,7,8].indexOf(vote) > -1
    res.promakh.token.voted = vote
    Tokens.update {value: res.promakh.token.value}, res.promakh.token
    Interview.findOne {}, {}, (e, doc) ->
      doc[vote] ||= 0
      doc[vote] = doc[vote] + 1
      Interview.update {}, doc

      res.render 'interview-result', values: convertInterviewToValues(doc)
  else
    Interview.findOne {}, {}, (e, doc) ->
      res.render 'interview-result', values: convertInterviewToValues(doc)

app.get '/counter.png', (req, res) ->
  gm(180, 40, "#ffffff")
  .drawText(10, 10, "promakh.ru")
  .drawText(10, 30, "Количество посещений: #{res.promakh.visitCounter}")
  .write COUNTER_FILE, (err) ->
    res.sendFile COUNTER_FILE, { root: path.join(__dirname) }

app.listen PORT, ->
  console.log 'Listen 3000...'

