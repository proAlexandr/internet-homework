CLASS_NAME = 'promakh-gallery'
CLASS_HELP = 'state-help'
COOKIE_FIRST_NAME = 'promakh-gallery-first-index'

getCookie = (name) ->
  matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "\\$1") + "=([^;]*)"))
  (if matches then decodeURIComponent(matches[1]) else `undefined`)

setCookie = (name, value, options) ->
  options = options or {}
  expires = options.expires
  if typeof expires is "number" and expires
    d = new Date()
    d.setTime d.getTime() + expires * 1000
    expires = options.expires = d
  options.expires = expires.toUTCString()  if expires and expires.toUTCString
  value = encodeURIComponent(value)
  updatedCookie = name + "=" + value
  for propName of options
    updatedCookie += "; " + propName
    propValue = options[propName]
    updatedCookie += "=" + propValue  if propValue isnt true
  document.cookie = updatedCookie
  return

getInternetExplorerVersion = ->
  rv = -1 # Return value assumes failure.
  if navigator.appName is "Microsoft Internet Explorer"
    ua = navigator.userAgent
    re = new RegExp("MSIE ([0-9]{1,}[.0-9]{0,})")
    rv = parseFloat(RegExp.$1)  if re.exec(ua)?
  rv

isIE8 = getInternetExplorerVersion() <= 8
isFirefox = navigator.userAgent.indexOf("Firefox") != -1

class Gallery
  constructor: (selector, options) ->
    @$el = $ selector
    body = $('body')

    @opened = yes
    @currentIndex = getCookie COOKIE_FIRST_NAME
    if !@currentIndex?
      @currentIndex = if options?.currentIndex then parseInt(options.currentIndex) else undefined

    @$elements = @$el.find 'a'
    @$elements.bind 'click', @show

    @$modal = $ @renderModal()
    @close()
    @$modal.appendTo body

    @$modalImage = @$modal.find '.image'
    @$modalOverlay = @$modal.find '.gallery-overlay'
    @$modalModal = @$modal.find '.gallery-modal'
    @$modalLoading = @$modal.find '.gallery-loading'
    @$modalClose = @$modal.find '.close'

    @$modal.bind 'click', @close
    @$modalOverlay.bind 'click', @close
    @$modalClose.bind 'click', @close

    body.bind 'keyup', @onKeyPress

    popStateEventName = if isIE8 then 'hashchange' else 'popstate'
    $(window).bind popStateEventName, @onpopstate

    @onresize()
    $(window).bind 'resize', @onresize

    if @currentIndex?
      index = @currentIndex
      $(window).trigger popStateEventName
      @_indexToHash index
      @open()

  onresize: =>
    @$modalModal.css 'height', (window.innerHeight - 100) + 'px'

  onpopstate: =>
    @currentIndex = @_currentIndexFromHash()
    if @currentIndex?
      @open()
      @_showCurrentIndex withoutHash: true
    else
      @close()

  open: (e) =>
    unless @opened
      @opened = yes
      @$modal.show()

  show: (e) =>
    e.preventDefault()
    $target = $ e.currentTarget
    @_indexToHash @$elements.index($target)
    @open()

  close: =>
    if @opened
      @opened = no
      @$modal.removeClass CLASS_HELP
      @$modal.hide()

  next: =>
    return unless @opened
    @_indexToHash @_nextIndex()

  prev: =>
    return unless @opened
    @_indexToHash @_prevIndex()

  help: =>
    @opened = true
    @$modal.addClass CLASS_HELP

  preloadImageWithIndex: (index) =>
    imagePath = $(@$elements[index]).attr 'href'
    image = new Image()
    image.src = imagePath

  makeFirst: =>
    setCookie COOKIE_FIRST_NAME, @currentIndex

#  save: =>
#    console.log('sdsd')
#    imagePath = $(@$elements[@currentIndex]).attr 'href'
#    $a = $ "<a href='#{imagePath}' target='_blank'>"
#    $a.appendTo $('body')
#    $a.trigger 'click'

  f5: (e) ->
    e.preventDefault()
    false

  onKeyPress: (e) =>
    switch e.keyCode
      when 27 then @close()      # esc
      when 39 then @next()
      when 37 then @prev()
      when 116 then @f5(e)      # F5
      when 83 then @makeFirst()  # s
      when 112, 72
        @$modal.show()
        @help()

  renderModal: (opts) =>
    """
<div class="gallery-modal-container">
  <div class='gallery-overlay'></div>
  <div class='gallery-modal'>
    <img class='image'>
    <div class="close">Закрыть</div>
    <div class='gallery-loading'>Loading...</div>
    <div class='gallery-help'>
      <h2>Помощь</h2>
      <ul>
        <li>F1, h - показать помошь</li>
        <li>Стрелка вправо - следующее изображение</li>
        <li>Стрелка влево - предыдущее изображение</li>
        <li>F5 - сохранить нас на текущей фотографии</li>
        <li>s - сделать картинку стартовой</li>
        <li>esc - закрыть</li>
      </ul>
    </div>
  </div>
</div>
"""

  _nextIndex: =>
    index = @currentIndex
    ++index
    if index >= @$elements.length then 0 else index

  _prevIndex: =>
    index = @currentIndex
    --index
    if index < 0 then @$elements.length - 1 else index

  _imageOnLoad: (index, imagePath)=>
    @$modalLoading.hide()
    @_applyImage index, imagePath
    @$modalImage.show()
    @preloadImageWithIndex @_nextIndex()
    @preloadImageWithIndex @_prevIndex()

  _showCurrentIndex: =>
    $a = $ @$elements[@currentIndex]
    imagePath = $a.attr 'href'
    @$modalImage.hide()

    image = new Image()
    index = @currentIndex
    image.src = imagePath
    @$modalLoading.show()
    if image.complete
      @_imageOnLoad(index, imagePath)
    else
      image.onload = =>
        @_imageOnLoad(index, imagePath)

  _applyImage: (index, path) =>
    if index != @currentIndex
      alert('fail')
      return
    @$modalImage.attr 'src', path

  _currentIndexFromHash: ->
    index = window.location.hash.substring(1, window.location.hash.length)
    if index then parseInt(index) else undefined

  _indexToHash: (index) ->
    window.location.hash = index.toString()


$ ->
  $('.' + CLASS_NAME).each (index, el) ->
    i = window.location.hash.substring(1, window.location.hash.length)
    new Gallery(el, currentIndex: i)