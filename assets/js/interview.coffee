CLASS_NAME = 'promakh-interview'

class Interview
  constructor: (selector, options) ->
    @$el = $ selector
    voted = @$el.data 'voted'
    @$el.hide()

    if voted == 1
      @what()
    else
      @$el.show()
      @$voteButtons = @$el.find '.vote'
      @$voteButtons.bind 'click', @vote

  vote: (e) =>
    target = e.currentTarget
    $.ajax
      url: '/interview'
      data: $(target).data()
      type: 'post'
      success: @showResults

  showResults: (data) =>
    @$el.html data
    setTimeout @tick, 2500

  what: =>
    $.ajax
      url: '/interview'
      type: 'get'
      success: (data) =>
        @$el.show()
        @showResults(data)

  tick: =>
    console.log 'tick'
    $.ajax
      url: '/interview'
      type: 'get'
      success: @showResults

$ ->
  $('.' + CLASS_NAME).each (index, el) ->
    new Interview el